var BatteryWidget = function () {


    var _battery = window.navigator.battery || window.navigator.mozBattery || window.navigator.webkitBattery;
    var _canvas = document.getElementById('BatteryWidget');
    var _ctx = undefined;

    if (_canvas.getContext) {
        _ctx = _canvas.getContext('2d');
    }


    _battery.onlevelchange = function(data) {
        console.log('Level changed '+data+'!');
        _drawBattery(_getStatus());
    };

    _battery.onchargingtimechange = function(evt) {
        console.log('Charging time changed ['+_getChargingTime()+']');
    };

    _battery.ondischargingtimechange = function(evt) {
        console.log('Discharging time changed ['+_getDischargingTime()+']');
    }

    var _getStatus = function () {
        var l = _battery.level;
        console.log('Charge level [' + l + ']')
        return l;
    };

    var _getDischargingTime = function() {
        var dt = _battery.dischargingTime;
        console.log('Decharing time is ['+ dt+']');
        return dt;
    };

    var _getChargingTime = function() {
        var ct = _battery.chargingTime;
        console.log('Charing time is ['+ ct+']');
        return ct;
    };


    /**
     *
     * @private
     */
    var _drawBattery = function (level) {
        _clearCanvas();
        _ctx.strokeStyle = 'rgba(0,255,0,1.0)';
        _ctx.strokeRect(10, 10, 250, 50);

        if (level < 0.15) {
            _ctx.fillStyle = 'rgba(255,0,0,1.0)';
        }
        else if (level < 0.25) {
            _ctx.fillStyle = 'rgba(255,255,0,1.0)';
        }
        else {
            _ctx.fillStyle = 'rgba(0,255,0,1.0)';
        }
        _ctx.fillRect(10, 10, _getStatus() * 250, 50);

        _ctx.fillStyle = 'rgba(0,0,0,1.0)';
        _ctx.font = '20pt Sans';

        _ctx.fillText(Number(level * 100).toFixed(1) + '%', 100, 50, 150);

        _ctx.font = '12pt Sans';

        var ct = _getChargingTime();
        if( ct === Infinity) {
            _ctx.fillText('Charging time: Fully loaded.', 10, 80,240);
        }
        else {
            var h = Number(ct/3600).toFixed(0);
            var m = Number(ct/60).toFixed(0);
            _ctx.fillText('Charging time: '+h+':'+m+'h.', 10,80,240);
        }

        var dt = _getDischargingTime();
        if( dt === Infinity) {
            _ctx.fillText('Discharging time: - (AC).', 10, 100,240);
        }
        else {
            var h = Number(dt/3600).toFixed(0);
            var m = Number(dt/60).toFixed(0);
            _ctx.fillText('Discharging time: '+h+':'+m+'h.', 10,100,240);
        }

    };

    var _clearCanvas = function () {
        _ctx.save();
        _ctx.setTransform(1, 0, 0, 1, 0, 0);
        _ctx.clearRect(0, 0, _ctx.canvas.width, _ctx.canvas.height);

        _ctx.fillStyle = 'rgba(255,255,255,1.0)';
        _ctx.fillRect(0, 0, _ctx.canvas.width, _ctx.canvas.height);

        // Restore the transform
        _ctx.restore();
    };
    /**
     *
     * @private
     */
    var _t = function () {
        console.log('Hallo');
        console.log('Battery [' + _battery + ']');
    }

    return {
        t: _t,
        drawBattery: _drawBattery,
        getStatus: _getStatus,
        getChargingTime: _getChargingTime,
        getDischargingTime: _getDischargingTime
    };
} || {};
