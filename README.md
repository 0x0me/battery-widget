# [HTML5 Battery Widget](https://bitbucket.org/0x0me/battery-widget)

HTML5 Battery Widget is a small demo application using the battery api and HTML5 canvas features.

* Source: [https://bitbucket.org/0x0me/battery-widget](https://bitbucket.org/0x0me/battery-widget)

## Features
* pure Javascript and HTML5 compliant battery widget